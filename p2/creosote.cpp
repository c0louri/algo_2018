#include <iostream>
using namespace std;

long long a , b, c;
int X[1000001];
unsigned long long Y[1000001];

long long calc (int f,int e) {
    long long temp = Y[e] - Y[f];
    return a*temp*temp + b * temp + c;
}

int main(int argc, char const *argv[]) {
    unsigned int N; // food
    cin >> N >> a >> b >> c;
    Y[0] = X[0] = 0;
    for (unsigned int i = 1; i <= N; i++) {
        cin >> X[i];
        Y[i] = Y[i-1] + X[i];
    }
    long long **A = new long long *[2];
    A[0] = new long long[N+1];
    A[1] = new long long[N+1];
    A[0][0] = 0;
    long long t1, t2;
    for (int j = 1; j <= N; j++) A[0][j] = calc(0,j);
    for (int i = 1; i <= N; i++) {
        for (int j = i; j <= N; j++) {
            t1 = A[0][j];
            t2 = A[0][i] + calc(i,j);
            if (t1 >= t2) A[1][j] = t1;
            else A[1][j] = t2;
        }
        swap(A[0], A[1]);
    }
    cout << A[0][N] << endl;
    return 0;
}
