#include <iostream>
using namespace std;


int find_nearest_smaller(long v[], int left, int right, long val)
{
    if (right < left) return -1;
    while (right - left > 1) {
        int mid = left + (right - left) / 2;
        if (v[mid] >= val) right = mid;
        else left = mid;
    }
    return right;
}

int find_nearest_bigger(long v[], int left, int right, long val)
{
    if (right < left) return -1;
    while (right - left > 1) {
        int mid = (left + right) / 2;
        if (v[mid] <= val) right = mid;
        else left = mid;
    }
    return right;
}

int main() {
    int N;
    cin >> N;
    long * s = new long[N];
    long * inc = new long[N];
    long * dec = new long[N];
    int * lr = new int[N];
    int * rl = new int[N];
    for (int i = 0; i < N; i++) cin >> s[i];
    if (N == 0) return 0;
    //Checking left-to-right (LIS)
    int length = 1;
    inc[0] = s[0];
    lr[0] = 1;
    for (int i = 1; i < N; i++) {
        if (s[i] < inc[0]) {
            inc[0] = s[i];
        }
        if (s[i] > inc[length - 1]) {
            inc[length++] = s[i];
        }
        else {
            int j = find_nearest_smaller(inc, -1, length - 1, s[i]);
            inc[j] = s[i];
        }
        lr[i] = length;
    }
    //Checking right-to-left (LDS)
    int rev_length = 1;
    dec[0] = s[N-1];
    rl[N-1] = 1;
    for (int i = N-2; i >= 0; i--) {
        if (s[i] > dec[0]) {
            dec[0] = s[i];
        }
        else if (s[i] < dec[rev_length - 1]) {
            dec[rev_length++] = s[i];
        }
        else {
            int j = find_nearest_bigger(dec, 0 , rev_length - 1, s[i]);
            dec[j] = s[i];
        }
        rl[i] = rev_length;
    }
    //Find best length
    int best = rl[0];
    for (int i= 0; i < N - 1; i++) {
        if (best < (lr[i]+rl[i+1])) best = lr[i] + rl[i+1];
    }
    if (best < lr[N-1]) best = lr[N-1];
    cout << best << endl;
    return 0;
}
