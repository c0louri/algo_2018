#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <tuple>
#include <unordered_map>
//https://cp-algorithms.com/graph/lca_tarjan.html
using namespace std;

long V, E;
long long none_mst = 0;
long long some_mst = 0;
long long all_mst = 0;
vector<vector<pair<long, long>>> edges_sorted;
vector<pair<long, long>> smt_edges;
unordered_map<unsigned long, int> smt_edges_weight;
unordered_map<unsigned long, bool> queries_checked;
unordered_map<unsigned long, char> edge; // only edges in smt
vector<vector<long>> adj;
vector<vector<pair<long, int>>> queries; // save query for each node
vector<long> ancestor;
vector<long> set_rank; // rank of set
vector<long> parent;
vector<pair<long, int>> upper_node;
vector<bool> visited;

unsigned long key(long x, long y) {
    return x * V + y;
}

int find(int v) {
    if (v == parent[v])
        return v;
    return parent[v] = find(parent[v]);
}

void union_rank(int a, int b) {
    a = find(a);
    b = find(b);
    if (a != b) {
        if (set_rank[a] < set_rank[b])
            swap(a, b);
        parent[b] = a;
        if (set_rank[a] == set_rank[b])
            set_rank[a]++;
    }
}

void update_edge_type(long s, long e, long lca, int edge_w) {
    // 2 -> all_mst
    // Always must s < e
    long v = s;
    long u = e;
    unsigned long k;
    vector<unsigned long> max_edge;
    while(v != lca) {
        int weight = upper_node[v].second;
        if (weight == edge_w) {
            if (v < upper_node[v].first) k = key(v, upper_node[v].first);
            else k = key(upper_node[v].first, v);
            max_edge.push_back(k);
        }
        v = upper_node[v].first;
    }
    while(u != lca) {
        int weight = upper_node[u].second;
        if (weight == edge_w) {
            if (u < upper_node[u].first) k = key(u, upper_node[u].first);
            else k = key(upper_node[u].first, u);
            max_edge.push_back(k);
        }
        u = upper_node[u].first;
    }
    if(max_edge.empty()) none_mst++;
    else {
        some_mst++;
        for (auto key = max_edge.begin(); key != max_edge.end(); key++) {
            if (edge[*key] != 1) {
                some_mst++;
                all_mst--;
                edge[*key] = 1;
            }
        }
    }

}

void dfs_tarjan(int v) {
    visited[v] = true;
    ancestor[v] = v;
    for (auto u = adj[v].begin(); u != adj[v].end(); u++) {
        if (!visited[*u]) {
            upper_node[*u].first = v;
            long a = v;
            long b = *u;
            if (a > b) swap(a, b);
            upper_node[*u].second = smt_edges_weight[key(a, b)];
            dfs_tarjan(*u);
            union_rank(v, *u);
            ancestor[find(v)] = v;
        }
    }
    for(auto dest = queries[v].begin(); dest != queries[v].end(); dest++) {
        if (visited[dest->first]) {
            long a = dest->first;
            long b = v;
            if (a > b) swap(a, b);
            if (queries_checked[key(a, b)] == true) continue;
            long lca = ancestor[find(dest->first)];
            update_edge_type(a, b, lca, dest->second);
            queries_checked[key(a, b)] = true;
        }
    }

}

void smt() {
    smt_edges.reserve(V-1);
    smt_edges_weight.reserve(V-1);
    // initialize disjoint data structure
    for (int i = 0; i < V; i++) {
        parent[i] = i;
        set_rank[i] = 0;
    }
    // Kruskall
    long edges_in_smt = 0;
    int weight = 0;
    for(auto it = edges_sorted.begin(); it != edges_sorted.end(); it++) {
        weight++;
        if (it->empty()) continue;
        if (edges_in_smt == V-1) {
            // update none_mst edges
            none_mst += it->size();
        } else {
            for(auto e = it->begin(); e != it->end(); e++) {
                // dont stop after finding smt
                // because all similar weight edges could be in one smt
                long a = e->first;
                long b = e->second;
                long x_root = find(a);
                long y_root = find(b);
                if (x_root != y_root) {
                    union_rank(x_root, y_root);
                    if (a > b) swap(a, b);
                    smt_edges.push_back(make_pair(a, b));
                    smt_edges_weight.insert(make_pair(key(a, b), weight));
                    edge.insert(make_pair(key(a, b), 0));
                    edges_in_smt++;
                    all_mst++;
                }
                else {
                    // Update queries
                    queries[a].push_back(make_pair(b, weight));
                    queries[b].push_back(make_pair(a, weight));
                    if (a > b) swap(a, b);
                    queries_checked.insert(make_pair(key(a, b), false));
                }
            }
        }
    }
}


void update_adj() {
    adj.reserve(V);
    for (int i = 0; i < V; i++) {
        vector<long> v;
        adj.push_back(v);
    }
    for(auto edge = smt_edges.begin(); edge != smt_edges.end(); edge++) {
        adj[edge->first].push_back(edge->second);
        adj[edge->second].push_back(edge->first);
    }
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> V >> E;
    // Initialize vectors' sizes
    ancestor.reserve(V);
    set_rank.reserve(V);
    parent.reserve(V);
    upper_node.reserve(V);
    visited.reserve(V);
    // Read edges
    edges_sorted.reserve(200);
    for (int i = 0 ; i < 200; i++) {
        vector<pair<long, long>> v;
        edges_sorted.push_back(v);
    }
    queries.reserve(V);
    queries_checked.reserve(E);
    edge.reserve(E);
    for (int i = 0; i < V; i++) {
        vector<pair<long, int>> v;
        queries.push_back(v);
    }
    for (int i = 0; i < E; i++) {
        long a, b;
        int c;
        cin >> a >> b >> c;
        edges_sorted[c-1].push_back(make_pair(a-1, b-1));
    }
    // Find SMT with Kruskal algorithm
    smt();
    // Update adj array for SMT
    update_adj();
    //initialize vectors again sector for Tarjan LCA
    for (int i = 0; i < V; i++) {
        parent[i] = i;
        set_rank[i] = 0;
        visited[i] = false;
        upper_node[i] = make_pair(i, 0);
    }
    ancestor.resize(V);
    dfs_tarjan(0);
    cout << all_mst << endl << none_mst << endl << some_mst << endl;
    return 0;
}
