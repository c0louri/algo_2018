#include <iostream>
#include <vector>
#include <utility>
using namespace std;

class Point{
    vector< pair<int,int> > crossings; // crossings with starting point x,y
public:
    int x,y;
    bool visited;
    int crosses; // crosses so far
    vector<long long> paths;
    Point() {
        visited = false;
        crosses = 0;
    }
    void init_paths_vector(int k) {
            paths.reserve(k+1);
    }
    void add_crossing(int end_x, int end_y) {
        crossings.push_back(make_pair(end_x, end_y));
    }
    vector<pair<int,int>> get_crossings() {
        return crossings;
    }
};

// Global variables
int N, M, K, X;
Point matrix[180][180];
vector<pair<int,int>> rev_top_order;


void dfs(int i, int j, int crossed) {
    if (matrix[i][j].visited == true) return;
    vector<pair<int,int>> cr = matrix[i][j].get_crossings();
    if (cr.empty()) {
        // If point NOT in the first row, move up
        if (i > 0) dfs(i-1, j, crossed);
        // If point NOT in the first collumn, move left
        if (j > 0) dfs(i, j-1, crossed);
    } else {
        if (crossed == X) {
            // already used max permitted crossings
            return;
        }
        for(auto edge = cr.begin(); edge != cr.end(); ++edge) {
            //matrix[(*edge).first][(*edge).second].crosses++;
            dfs((*edge).first, (*edge).second, crossed + 1);
        }
    }
    // mark point as visited
    matrix[i][j].visited = true;
    rev_top_order.push_back(make_pair(i,j));
}

void topol_sort(int start_x, int start_y) {
    dfs(start_x, start_y, 0);
    // Print reversed topological order
    //for (auto it = rev_top_order.begin(); it != rev_top_order.end(); ++it) {
    //    cout << (*it).x << ", " << (*it).y << endl;
    //}
}

long long count_paths() {
    matrix[0][0].paths[0] = 1;
    int cr_count = 0;
    for(auto it = rev_top_order.begin(); it != rev_top_order.end(); ++it) {
        int x = (*it).first;
        int y = (*it).second;
        vector<pair<int,int>> cr = matrix[x][y].get_crossings();
        if (cr.empty()) {
            if (x > 0) {
                for(int i = 0; i <=cr_count; i++)
                    matrix[x][y].paths[i] += (matrix[x - 1][y].paths[i] % 1000000103);
            }
            if (y > 0) {
                for(int i = 0; i <=cr_count; i++)
                    matrix[x][y].paths[i] += (matrix[x][y - 1].paths[i] % 1000000103);
            }
        }
        else {
            for(auto edge = cr.begin(); edge != cr.end(); ++edge) {
                if (cr_count < X) cr_count++;
                for(int i = 0; i < cr_count; i++)
                    matrix[x][y].paths[i + 1] =
                        matrix[(*edge).first][(*edge).second].paths[i];
            }
        }
    }
    long long sum_paths = 0;
    for (int i = 0; i <= cr_count; i++) sum_paths += (matrix[N-1][M-1].paths[i] % 1000000103);
    return sum_paths % 1000000103;
}

int main(int argc, char const *argv[]) {
    cin >> N >> M >> K >> X;
    // Fill Point objects
    for(int i = 0; i < N ; i++) {
        for(int j = 0; j < M ; j++) {
            matrix[i][j].x = i;
            matrix[i][j].y = j;
            matrix[i][j].init_paths_vector(X);
        }
    }// Check if a exist a path with max X crossings by checking if in the topological
    // order exists the end node
    // Add crossings
    for(int i = 0; i < K; i++) {
        int s, e, s_x, s_y, e_x, e_y;
        cin >> s >> e;
        s_x = s / M;
        s_y = s % M;
        e_x = e / M;
        e_y = e % M;
        matrix[s_x][s_y].add_crossing(e_x, e_y);
    }
    // Topological Sorting
    topol_sort(N-1, M-1);
    // Check if a exist a path with max X crossings by checking if in the topological
    // order exists the end node
    int end_x = rev_top_order[0].first;
    int end_y = rev_top_order[0].second;
    if (end_x == 0 && end_y == 0) cout << count_paths() << endl;
    else cout << 0 << endl;
    return 0;
}
