#include <iostream>

using namespace std;

//1st collumn is start-time
//2nd collumn is speed
//3nd collumn is 0 or 1, 1 if it took part in collision,all initialized with 0

// long long A[200000][3];
// long long B[200000][3];

double min(double a, double b) {
    if (a <= b) return a;
    else return b;
}

long find_fastest_part(long long A[][3], double x, long N, double upper_limit) {
    double best_time = 1000000000000.0;
    long best_par;
    double calc;
    for (long i = 0; i < N; i++) {
        if (A[i][2] != 0) continue;
        if (A[i][0] > min(upper_limit,best_time)) break;
        calc = (x / (double)(A[i][1])) + A[i][0];
        if (calc < best_time) {
            best_time = calc;
            best_par = i;
        }
    }
    return best_par;
}

int main(int argc, char const *argv[]) {
    long N;  // number of particles
    long long L; // distance between shooters
    int K;  // number of pairs to be found
    cin >> N >> L >> K;
    auto A = new long long[N][3];
    auto B = new long long[N][3];
    //read data for A
    for (long i = 0; i < N; i++) {
        cin >> A[i][0] >> A[i][1];
        A[i][2] = 0;
    }
    for (long i = N; i < 2*N; i++) {
        cin >> B[i - N][0] >> B[i - N][1];
        B[i-N][2] = 0;
    }
    for (int k = 0; k < K; k++) {
        double left = 0;
        double right = L;
        double x;
        double upper_limit = 1000000000000.0;
        while (left != right) {
            x = (right + left) / 2;
            //finding the smaller t for reaching x from left
            long best_par_A =find_fastest_part(A,x, N, upper_limit);
            double best_time_A = (x  / (double)(A[best_par_A][1])) + A[best_par_A][0];
            //finding the smaller t for reaching x from right
            long best_par_B =find_fastest_part(B,L - x, N, upper_limit);
            double best_time_B = ((L - x) / (double)(B[best_par_B][1])) + B[best_par_B][0];
            //check which side reached first x
            if (right - left <= 1) {
                cout << best_par_A + 1 << " " << best_par_B + 1 << endl ;
                A[best_par_A][2] = 1;
                B[best_par_B][2] = 1;
                break;
            }
            // upper_limit = best_time_A + best_time_B;
            if (best_time_A < best_time_B) {
                //A reached x first
                //the coliision will happen in [left,x]
                left = x;
                upper_limit = best_time_B;
            }
            else if (best_time_A > best_time_B) {
                //B reached x first
                //the collision will happen in [x,right]
                right = x;
                upper_limit = best_time_A;
            }
            else {
                //Collision of A and B!!
                cout << best_par_A + 1 << " " << best_par_B + 1 << endl;
                A[best_par_A][2] = 1;
                B[best_par_B][2] = 1;
                break;
            }
        }
    }
    return 0;
}
