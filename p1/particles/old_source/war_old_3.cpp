#include <iostream>
#include <fstream>
#include <climits>
#include <tuple>

using namespace std;

//1st collumn is start-time
//2nd collumn is speed
//3nd collumn is 0 or 1, 1 if it took part in collision, all  initialized to 0

unsigned long A[200000][3];
unsigned long B[200000][3];

long find_pA_with_best_time(double x, long N) {
    double best_time_A = 1000000000.0;
    long best_par_A;
    double calc;
    for (long i = 0; i < N; i++) {
        if (A[i][2] != 0) continue;
        if (A[i][0] > best_time_A) break;
        calc = (x + A[i][1] * A[i][0]) / double(A[i][1]);
        if (calc < best_time_A) {
            best_time_A = calc;
            best_par_A = i;
        }
    }
    return best_par_A;
}

long find_pB_with_best_time(double x, long N) {
    double best_time_B = 1000000000.0;
    long best_par_B;
    double calc;
    for (long i = 0; i < N; i++) {
        if (B[i][2] != 0) continue;
        if (B[i][0] > best_time_B) break;
        calc = (x + B[i][1] * B[i][0]) / double(B[i][1]);
        if (calc < best_time_B) {
            best_time_B = calc;
            best_par_B = i;
        }
    }
    return best_par_B;
}

int main(int argc, char const *argv[]) {
    long N;             // number of particles
    unsigned long L;    // distance between shooters
    int K;              // number of pairs to be found
    cin >> N >> L >> K;

    for (long i = 0; i < N; i++) {
        cin >> A[i][0] >> A[i][1];
        A[i][2] = 0;
    }

    for (long i = N; i < 2*N; i++) {
        cin >> B[i - N][0] >> B[i - N][1];
        B[i-N][2] = 0;
    }

    for (int k = 0; k < K; k++) {
        unsigned long left = 0;
        unsigned long right = L;
        double x;
        long best_par_A, best_par_B;
        double best_time_A, best_time_B;
        while (left != right) {
            x = (right + left) / 2;
            //finding the smaller t for reaching x from left
            best_par_A =find_pA_with_best_time(x, N);
            best_time_A = (x + A[best_par_A][1] * A[best_par_A][0]) / double(A[best_par_A][1]);
            //finding the smaller t for reaching x from right
            best_par_B =find_pB_with_best_time(L - x, N);
            best_time_B = (L - x + B[best_par_B][1] * B[best_par_B][0]) / double(B[best_par_B][1]);
            //check which side reached first x
            if (right - left <= 1) {
                cout << best_par_A + 1 << " " << best_par_B + 1 << endl ;
                A[best_par_A][2] = 1;
                B[best_par_B][2] = 1;
                break;
            }
            if (best_time_A < best_time_B) {
                //A reached x first
                //the coliision will happen in [x,right]
                left = x;
            }
            else if (best_time_A > best_time_B) {
                //B reached x first
                //the collision will happen in [left,x]
                right = x;
            }
            else {
                //Collision of A and B!!
                cout << best_par_A + 1 << " " << best_par_B + 1 << endl;
                A[best_par_A][2] = 1;
                B[best_par_B][2] = 1;
                break;
            }
        }
    }
    return 0;
}
