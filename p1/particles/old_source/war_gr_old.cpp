#include <iostream>

using namespace std;

//1st collumn is start-time
//2nd collumn is speed
//3nd collumn is 0 or 1, 1 if it took part in collision,all initialized with 0

long long A[200000][3];
long long B[200000][3];
// byte Aa[200000];
// byte Bb[200];

long find_pA_with_best_time(double x, long N, double upper_limit) {
    double best_time_A = upper_limit;
    long best_par_A;
    double calc;
    for (long i = 0; i < N; i++) {
        if (A[i][2] == 0) continue;
        if (A[i][0] > (best_time_A)) break;
        calc = (x + A[i][2]) / (double)(A[i][1]);
        if (calc < best_time_A) {
            best_time_A = calc;
            best_par_A = i;
        }
    }
    return best_par_A;
}

long find_pB_with_best_time(double x, long N, double upper_limit) {
    double best_time_B = upper_limit;
    long best_par_B;
    double calc;
    for (long i = 0; i < N; i++) {
        if (B[i][2] == 0) continue;
        if (B[i][0] > (best_time_B)) break;
        calc = (x + B[i][2]) / (double)(B[i][1]);
        if (calc < best_time_B) {
            best_time_B = calc;
            best_par_B = i;
        }
    }
    return best_par_B;
}


int main(int argc, char const *argv[]) {
    long N;  // number of particles
    long long L; // distance between shooters
    int K;  // number of pairs to be found

    cin >> N >> L >> K;
    //read data for A
    for (long i = 0; i < N; i++) {
        cin >> A[i][0] >> A[i][1];
        A[i][2] = A[i][0] * A[i][1];
    }
    //read data for B
    for (long i = N; i < 2*N; i++) {
        cin >> B[i - N][0] >> B[i - N][1];
        B[i-N][2] = B[i-N][0] * B[i-N][1];
    }
    //
    for (int k = 0; k < K; k++) {
        long long left = 0;
        long long right = L;
        double x;
        double upper_limit = 1000000000.0;
         //long double upper_limitB = 10000000000.0;
        while (left != right) {

            // double upper_limit = 1000000000.0;
            x = (right + left) / 2;
            //finding the smaller t for reaching x from left
            long best_par_A =find_pA_with_best_time(x, N, upper_limit);
            double best_time_A = (x + A[best_par_A][2]) / (double)(A[best_par_A][1]);
            //finding the smaller t for reaching x from right
            long best_par_B =find_pB_with_best_time(L - x, N, upper_limit);
            double best_time_B = (L - x + B[best_par_B][2]) / (double)(B[best_par_B][1]);
            //check which side reached first x
            if (right - left <= 1) {
                cout << best_par_A + 1 << " " << best_par_B + 1 << endl ;
                A[best_par_A][2] = 0;
                B[best_par_B][2] = 0;
                break;
            }

            upper_limit = best_time_A + best_time_B;
            if (best_time_A < best_time_B) {
                //A reached x first
                //the coliision will happen in [left,x]
                left = x;
            }
            else if (best_time_A > best_time_B) {
                //B reached x first
                //the collision will happen in [x,right]
                right = x;
            }
            else {
                //Collision of A and B!!
                cout << best_par_A + 1 << " " << best_par_B + 1 << endl;
                A[best_par_A][2] = 0;
                B[best_par_B][2] = 0;
                break;
            }
        }
    }
    return 0;
}
