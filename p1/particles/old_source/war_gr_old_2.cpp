#include <iostream>
#include <fstream>
#include <climits>

using namespace std;

//1st collumn is start-time
//2nd collumn is speed
//3nd collumn is 1st coll * 2nd coll


unsigned long A[200000][3];
unsigned long B[200000][3];


int main(int argc, char const *argv[]) {
    //1st argument input
    //2nd argument output
    // ifstream inp, right_file;

    // inp.open(argv[1]);
    // cout << "Read input...\n";
    // right_file.open(argv[2]);
    // cout << "Read output...\n";

    long N;  // number of particles
    unsigned long L; // distance between shooters
    int K;  // number of pairs to be found

    // cin >> N >> L >> K;
    cin >> N >> L >> K;
    // unsigned long A[N][3];
    // unsigned long B[N][3];
    //firstly for a
    for (long i = 0; i < N; i++) {
        cin >> A[i][0] >> A[i][1];
        A[i][2] = 0;
        // cout << i << " ) " << A[i][0] << " " << A[i][1] << endl; // " " << A[i][2] << endl;
    }
    //secondly
    for (long i = N; i < 2*N; i++) {
        cin >> B[i - N][0] >> B[i - N][1];
        B[i-N][2] = 0;
        // cout << i << " ) " << B[i - N][0] << " " << B[i - N][1] << endl; // " " << B[i][2] << endl;
    }


    double calc;
    // cout << " Pairs :\n";
    for (int k = 0; k < K; k++) {
        // cout << k << ") ";
        unsigned long left = 0;
        unsigned long right = L;
        double x;
        while (left != right) {
            // cout << "[" << left << ", " << right << "]\n";
            x = (right + left) / 2;

            // cout << "New X = " << x << endl;
            //finding the smaller t for reaching x from left
            double best_time_A = 1000000000.0;
            long best_par_A;
            for (long i = 0; i < N; i++) {
                if (A[i][2] != 0) continue;
                calc = (x + A[i][1] * A[i][0]) / double(A[i][1]);
                if (calc < best_time_A) {
                    best_time_A = calc;
                    best_par_A = i;
                }
            }
            //finding the smaller t for reaching x from right
            double best_time_B = 1000000000.0;
            long best_par_B;
            double y = L - x;
            for (long i = 0; i < N; i++) {
                if (B[i][2] != 0) continue;
                calc = (y + B[i][1] * B[i][0]) / double(B[i][1]);
                if (calc < best_time_B) {
                    best_time_B = calc;
                    best_par_B = i;
                }
            }
            //check which side reached first x
            // cout << best_time_A << " " << best_time_B << endl;
            // cout << best_par_A + 1 << " " << best_par_B + 1 << endl ;
            if (right - left <= 1) {
                cout << best_par_A + 1 << " " << best_par_B + 1 << endl ;
                A[best_par_A][2] = 1;
                B[best_par_B][2] = 1;
                //check if it right
                /*long mpe1,mpe2;
                right_file >> mpe1 >>mpe2;
                if (((best_par_A + 1) != mpe1) || ((best_par_B + 1) != mpe2)) {
                    cout << "WRONG ANSWER!\n";
                    exit(1);
                }*/
                break;
            }
            if (best_time_A < best_time_B) {
                //A reached x first
                //the coliision will hapen in [x,right]
                left = x;
            }
            else if (best_time_A > best_time_B) {
                //B reached x first
                //the collision will hapeen in [left,x]
                right = x;
            }
            else {
                //Collision of A and B!!
                cout << best_par_A + 1 << " " << best_par_B + 1 << endl;
                A[best_par_A][2] = 1;
                B[best_par_B][2] = 1;
                //check if it right
                /*long mpe1,mpe2;
                right_file >> mpe1 >>mpe2;
                if (((best_par_A + 1) != mpe1) || ((best_par_B + 1) != mpe2)) {
                    cout << "WRONG ANSWER!\n";
                    exit(1);
                }*/
                break;
            }
        }
    }
    // cout << "SUCCESS!!\n";
    return 0;
}
