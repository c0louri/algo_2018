#include <iostream>
#include <stack>
#include <utility>
#include <climits>

using namespace std;

long long min_cost;
long long H[1000000];
unsigned long long L[1000000], R[1000000];

void populateL(long N) {

    stack<pair<long long, long>> s;
    pair<long long,long> temp;

    s.push(make_pair(H[0],0));
    L[0] = H[0];

    for(long i = 1; i < N; i++) {
        if (H[i] <= H[i-1]) {
            L[i] = L[i-1] + H[i];
        }
        else {
            temp = s.top();
            while( H[i] >= temp.first) {
                s.pop();
                if (s.empty()) break;
                else {
                    temp = s.top();
                }
            }
            if (!s.empty())
                L[i] = L[s.top().second] + H[i] * (i - s.top().second);
            else L[i] = H[i] * (i+1);
        }
        s.push(make_pair(H[i],i));
    }
}

void populateR(long N) {

    stack<pair<long long, long>> s;
    pair<long long,long> temp;

    s.push(make_pair(H[N-1],N-1));
    R[N-1] = H[N-1];

    for(long i = N-2; i >= 0; i--) {
        if (H[i] <= H[i+1]) {
            R[i] = R[i+1] + H[i];
        }
        else {
            temp = s.top();
            while( H[i] >= temp.first) {
                s.pop();
                if (s.empty()) break;
                else {
                    temp = s.top();
                }
            }
            if (!s.empty())
                R[i] = R[s.top().second] + H[i] * (s.top().second - i);
            else R[i] = H[i] * (N - i);
        }
        s.push(make_pair(H[i],i));
    }
}


int main(int argc, char const *argv[]) {
    long N;
    cin >> N;
    for (long i = 0; i < N; i++) cin >> H[i];
    min_cost = LLONG_MAX;
    populateL(N);
    populateR(N);
    long long cost;
    for (long i = 0; i < N ; i++) {
        cost = L[i] + R[i] - H[i];
        if (cost < min_cost) min_cost = cost;
    }
    cout << min_cost << endl;
    /*for (int i = 0 ; i < N; i++) {
        cout << L[i] << " ";
    }
    cout << endl;
    for (int i = 0 ; i < N; i++) {
        cout << R[i] << " ";
    }
    cout << endl;*/
    return 0;
}
