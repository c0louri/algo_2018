#include <iostream>
// #include <fstream>
#include <climits>

using namespace std;

unsigned long long min_cost;
long long H[1000000];

long long find_max(long long A[], int left, int right) {
    long long max = 0;
    int max_pos = 0;
    for(int i = left; i <= right; i++) {
        if (A[i] > max) {
            max = A[i];
            max_pos = i;
        }
    }
    return max_pos;
}

void check(long long A[], int left, int right, unsigned long long cost) {

    int pos_max = find_max(A, left, right);
    unsigned long long new_cost = cost + A[pos_max] * (right-left + 1);
    if (new_cost < min_cost) {
        min_cost = new_cost;
    }

    //aristero diasthma
    if (pos_max > left)
        check(A, left, pos_max - 1, cost + A[pos_max] * (right - pos_max + 1));

    //deksio diasthma
    if (pos_max < right)
        check(A, pos_max + 1, right, cost + A[pos_max] * (pos_max - left + 1));
}


int main(int argc, char* argv[]) {
    int N;
    cin >> N;
    for (int i = 0; i < N; i++) cin >> H[i];
    min_cost = LLONG_MAX;
    check(H, 0, N-1, 0);
    cout << min_cost << endl;
    return 0;
}
