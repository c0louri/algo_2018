#include <iostream>
#include <limits>
using namespace std;

#define INFINITE numeric_limits<long>::max()
long dist[101][101][11];

//inline b
int main(int argc, char const *argv[]) {
    int N, M, K, Q;
    cin >> N >> M >> K >> Q;
    // fill dist array with INFINITE values
    for (int i = 1; i <= N; i++)
        for (int j = 1; j <= N; j++)
            for (int k = 0; k <= K; k++)
                dist[i][j][k] = INFINITE;
    // Diagonial values = 0 (w(i,i)=0)
    for (int i = 1; i <= N; i++)
        for (int k = 0; k <= K; k++)
            dist[i][i][k] = 0;
    // save adjacency matrix
    for (int i = 0; i < M; i++) {
        int u ,v;
        long d;
        cin >> u >> v >> d;
        // Update for FW without exceptions
        dist[u][v][0] = d;
        // Update for paths with exceptions
        // Initialize reverse movement weight
        dist[v][u][1] = d;
    }
    for(int e = 0; e <= K; e++) {
        for (int k = 1; k <= N; k++) {
            for (int u = 1; u <= N; u++) {
                for (int v = 1; v <= N; v++) {
                    for (int a = 0; a <= e; a++) {
                        if (dist[u][k][a] == INFINITE || dist[k][v][e-a] == INFINITE) continue;
                        if (dist[u][v][e] > dist[u][k][a] + dist[k][v][e-a]) {
                            dist[u][v][e] = dist[u][k][a] + dist[k][v][e-a];
                        }
                    }
                }
            }
        }
    }
    // Check queries
    for (int q = 0; q < Q; q++) {
        int a, b, c;
        cin >> a >> b >> c;
        if (dist[a][b][c] == INFINITE) cout << "IMPOSSIBLE\n";
        else cout << dist[a][b][c] << endl;
    }
    return 0;
}
