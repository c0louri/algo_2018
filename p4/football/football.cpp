#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
vector<vector<long>> adj;
vector<bool> discovered;
long disc;
long N;


void dfs(long s) {
    discovered[s] = true;
    disc++;
    for(auto it = adj[s].begin(); it != adj[s].end(); it++) {
        if (!discovered[*it]) dfs(*it);
    }
}


int main(int argc, char const *argv[]) {
    cin >> N;
    adj.reserve(N);
    discovered.resize(N);
    // Initialize vectors
    for(long i = 0; i < N; i++) {
        vector<long> v;
        adj.push_back(v);
        discovered[i] = false;
    }
    for(long i = 0; i < N; i++) {
        long count;
        cin >> count;
        for(long e = 1; e <= count; e++){
            int v; // team v wins team i ALWAYS
            cin >> v;
            adj[v-1].push_back(i);
        }
    }
    /*int i = 0;
    for (auto it = adj.begin(); it != adj.end(); ++it) {
        cout << "Team " << ++i << " wins:\n";
        for(auto it2 = it->begin(); it2 != it->end(); it2++) {
            cout << *it2 + 1 << " ";
        } cout << endl << endl;
    }*/
    long sure_winners = 0;
    // run dfs for each team
    for(long s = 0; s < N; s++) {
        fill(discovered.begin(), discovered.end(), false);
        disc = 0;
        dfs(s);
        if (disc == N) sure_winners++;
    }
    cout << sure_winners << endl;
    return 0;
}
