#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
vector<vector<long>> adj;

// Tarjan algorithm
// en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
// Variables for Tarjan algorithm for Strongly Connected Components
// Variable names are the same with the ones in wikipedia pseudocode
vector<long> v_index;
// if v_index value is < 0 means it is UNDEFINED
vector<long> lowlink;
vector<bool> onStack;
vector<long> Stack;
long ind; // smallest unused v_index value for marking vertices
long scc_count; // number of scc in the graph
vector<long> in_scc; // saving in which SCC belong i-th vertex
vector<long> vertices_in_scc;
vector<bool> root_scc;


void strongconnect(long v) {
    v_index[v] = ind; // = with the smallest unused v_index
    lowlink[v] = ind;
    ind++;
    Stack.push_back(v);
    onStack[v] = true;
    for(auto it = adj[v].begin(); it != adj[v].end(); it++) {
        long w = *it;
        if (v_index[w] < 0) {
            strongconnect(w);
            lowlink[v] = min(lowlink[v], lowlink[w]);
        }
        else if (onStack[w]) {
            // w is in Stack (in current scc)
            lowlink[v] = min(lowlink[v], v_index[w]);
        }
    }
    if (lowlink[v] == v_index[v]) {
        long count = 0;
        long w;
        do {
            w = Stack.back();
            Stack.pop_back();
            onStack[w] = false;
            in_scc[w] = scc_count;
            count++;
        } while(w != v);
        vertices_in_scc.push_back(count);
        scc_count++;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    long nV;
    cin >> nV;
    adj.reserve(nV);
    Stack.reserve(nV);
    v_index.resize(nV);
    lowlink.resize(nV);
    onStack.resize(nV);
    in_scc.resize(nV);
    // initialize v_index, lowlink, onStack, in_scc
    for(int i = 0; i < nV; i++) {
        v_index[i] = -1;
        lowlink[i] = -1;
        onStack[i] = false;
        in_scc[i] = -1;
    }
    // Initialize vectors and store edges in adj lists(vectors)
    for(long i = 0; i < nV; i++) {
        vector<long> v;
        adj.push_back(v);
    }
    for(long i = 0; i < nV; i++) {
        long count;
        cin >> count;
        for(long e = 1; e <= count; e++){
            int v; // team v wins team i ALWAYS
            cin >> v;
            adj[v-1].push_back(i);
        }
    }
    // Find Strongly Connected Components
    ind = 0;
    scc_count = 0;
    for(long v = 0; v < nV; v++) {
        if (v_index[v] < 0) strongconnect(v);
    }
    // Initializing root_scc for marking root SCCs
    root_scc.resize(scc_count);
    for(long i = 0; i < scc_count; i++) {
        root_scc[i] = true;
    }
    // Find edges between SCCs
    // if an edge exists bwtween 2 SCCs then the teams of the destination
    // SCC are not going to be included in the final solution
    for(long i = 0; i < nV; i++) {
        for(auto it = adj[i].begin(); it != adj[i].end(); it++) {
            if(in_scc[i] != in_scc[*it]) {
                // edge is between 2 SCCs
                root_scc[in_scc[*it]] = false;
            }
        }
    }
    long sure_winners = 0;
    int j = 0;
    for(int i = 0; i < scc_count; i++) {
        if(root_scc[i]) {
            j++;
            if (j > 1) {
                sure_winners = 0;
                break;
            }
            sure_winners += vertices_in_scc[i];
        }
    }
    cout << sure_winners << endl;
    return 0;
}
